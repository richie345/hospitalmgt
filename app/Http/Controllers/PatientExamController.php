<?php

namespace App\Http\Controllers;

use App\Models\PatientExam;
use App\Models\Patient;
use App\Models\Schedule;
use App\Models\Symptoms;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

class PatientExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function addPatientExamView($patientid, $schid)
    {
        $patient = Patient::where(['patient_id'=>$patientid])->first();
        $schedules = Schedule::find($schid);
        $symptoms = Symptoms::all();
        return view('admin.patients.add_patient_exam',compact('patient','schedules','symptoms'));
    }

    public function changeValidity($id){

        try{

            $update = PatientExam::find($id);
            if($update->valid == "Yes"){
                $update->valid = "No";
            }else{
                $update->valid = "Yes";
            }
            $update->save();
           
      if($update){
           
            return redirect()->route('view_patient')->with('success', "Patient Diagnosis Validity set! ");
        }
        
        return redirect()->back()->with('error', "Error. Something went wrong, unable to update validity.");
        
  }
    catch(\Exception $e){
         //dd($e);
        return redirect()->back()->with('error', "Error. Something went wrong, unable to update validity. Try again or contact system administrator.");
    }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $patient_id = $request->patient_id;
            $title = $request->title;
            $presenting_complain = $request->presenting_complain;
            $history_of_present_illness = $request->history_of_present_illness;
            $allergies = $request->allergies;
            $past_medical_history = $request->past_medical_history;
            $family_history = $request->family_history;
            $social_history = $request->social_history;
            $drug_history = $request->drug_history;
            $occupational_history = $request->occupational_history;
            //$review_of_systems = $request->review_of_systems;
            $physical_mental_examination = $request->physical_mental_examination;
            $lab = $request->lab;
            $assessment = $request->assessment;
            $carried_out_by = auth()->user()->id;
            $entry_id = $request->entry_id;
            $plan = $request->plan;
            $date_carried_out = Carbon::now();
            $remarks = $request->remarks;
    
            
            $patientArray['patient_id'] = $patient_id;
            $patientArray['title'] = $title;
            $patientArray['presenting_complain'] = $presenting_complain;
            $patientArray['history_of_present_illness'] = $history_of_present_illness;
            $patientArray['allergies'] = $allergies;
            $patientArray['past_medical_history'] = $past_medical_history;
            $patientArray['family_history'] = $family_history;
            $patientArray['social_history'] = $social_history;
            $patientArray['drug_history'] = $drug_history;
            $patientArray['occupational_history'] = $occupational_history;
            $patientArray['review_of_systems'] = "";
            $patientArray['physical_mental_examination'] = $physical_mental_examination;
            $patientArray['lab'] = $lab;
            $patientArray['assessment'] = $assessment;
            $patientArray['carried_out_by'] = $carried_out_by;
            $patientArray['entry_id'] = $entry_id;
            $patientArray['plan'] = $plan;
            $patientArray['date_carried_out'] = $date_carried_out;
            $patientArray['remarks'] = $remarks;
            $patientArray['valid'] = "Yes";
            

            // save the patient
            $patient = PatientExam::create($patientArray);
            
           
      if($patient){
           
            return redirect()->route('patients')->with('success', "New Patient Examination done successfully! ");
        }
        
        return redirect()->back()->with('error', "Error. Something went wrong, unable to create Patient Examination.");
        
  }
    catch(\Exception $e){
         //dd($e);
        return redirect()->back()->with('error', "Error. Something went wrong, unable to create patient examination. Try again or contact system administrator.");
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
