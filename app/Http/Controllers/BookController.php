<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Storage;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::all();
        return view('admin.books.books',compact('books'));
    }

    public function borrowedBooks()
    {
        //$books = Book::where(['borrowed'=>"yes"])->get();
        $books = DB::table('books')
                     ->join('book_lendings', 'books.book_id', '=', 'book_lendings.book_id')
                     ->select('books.*','book_lendings.date_borrowed','book_lendings.user_id',
                              'book_lendings.date_expected','book_lendings.date_returned')
                     ->where('books.borrowed', '=', 'yes')
                     ->get();
        return view('admin.books.borrowed_books',compact('books'));
    }

    public function addBookView()
    {
        //$books = Book::where(['borrowed'=>"yes"])->get();
        return view('admin.books.add_book');
    }

    public function availableBooks()
    {
        $books = Book::where(['borrowed'=>"no"])->get();
        
        return view('admin.books.available_books',compact('books'));
    }

    public function myBorrowedBooks()
    {
        //$books = Book::where(['borrowed'=>"yes"])->get();
        $books = DB::table('books')
                     ->join('book_lendings', 'books.book_id', '=', 'book_lendings.book_id')
                     ->select('books.*','book_lendings.date_borrowed','book_lendings.user_id',
                              'book_lendings.date_expected','book_lendings.date_returned')
                     ->where('books.borrowed', '=', 'yes')
                     ->get();
        return view('admin.books.borrowed_books',compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*$this->validate($request, [
            'last_name' => 'required|string',
            'first_name' => 'required|string',
            'email' => 'required|string|max:255|unique:users',
            
         ]);*/
         //dd($request->all());

        try{
                $book_id = $request->book_id;
                $booktitle = $request->booktitle;
                $isbn = $request->isbn;
                $revisionno = $request->revisionno;
                $publisher = $request->publisher;
                $publishdate = $request->publishdate;
                $author = $request->author;
                $genre = $request->genre;
                $borrowed = $request->borrowed;
                
                $bookArray['book_id'] = $book_id;
                $bookArray['booktitle'] = $booktitle;
                $bookArray['uuid'] = uniqid();
                $bookArray['isbn'] = $isbn;
                $bookArray['revision_number'] = $revisionno;
                $bookArray['publisher'] = $publisher;
                $bookArray['published_date'] = $publishdate;
                $bookArray['coverimage_url'] = "test";
                $bookArray['dateadded'] = Carbon::now();
                $bookArray['author'] = $author;
                $bookArray['genre'] = $genre;
                $bookArray['deleted'] = 0;
                $bookArray['borrowed'] = $borrowed;

                $file = request()->file('file');
	
                $fullname = $file->getClientOriginalName();

                $target_path = "uploads";

                $target_path1 = "/app/uploads";

                $target_file1= storage_path().$target_path1. "/" . $book_id.".png";

                $bookArray['coverimage_url'] = $book_id.".png";


                Storage::putFileAs( $target_path, $file, $fullname );

                // save the book
                $book = Book::create($bookArray);
                
               
          if($book){
               
                return redirect()->route('books')->with('success', "New Book successfully! ");
            }
            
            return redirect()->back()->with('error', "Error. Something went wrong, unable to create book.");
            
      }
        catch(\Exception $e){
             //dd($e);
            return redirect()->back()->with('error', "Error. Something went wrong, unable to create book. Try again or contact system administrator.");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        //
    }
}
