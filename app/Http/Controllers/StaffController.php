<?php

namespace App\Http\Controllers;

use App\Models\Staff;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Storage;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staff = Staff::all();
        return view('admin.patients.staff',compact('staff'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{
            $staff_id = $request->staff_id;
            $title_id = $request->title_id;
            $surname = $request->surname;
            $firstname = $request->firstname;
            $othername = $request->othername;
            $address = $request->address;
            $gender = $request->gender;
            $phone = $request->phone;
            $dob = $request->dob;
            $dept = $request->dept;
            $division = $request->division;
            $unit = $request->unit;
            $qualification = $request->qualification;
            $position = $request->position;
            $registered_by = auth()->user()->id;
            $salary_scale = $request->salary_scale;
            $specialization = $request->specialization;
            $email = $request->email;
            $occupation = $request->occupation;
            $mstatus = $request->mstatus;
            $nospouse = $request->nospouse;
            $nationality = $request->nationality;
            $religion = $request->religion;
            $staff_status = $request->staff_status;
            $staff_category = $request->staff_category;

            
            $patientArray['staff_id'] = $staff_id;
            $patientArray['title_id'] = $title_id;
            //$patientArray['uuid'] = uniqid();
            $patientArray['surname'] = $surname;
            $patientArray['firstname'] = $firstname;
            $patientArray['othername'] = $othername;
            $patientArray['address'] = $address;
            $patientArray['gender'] = $gender;
            $patientArray['dateregistered'] = Carbon::now();
            $patientArray['phone'] = $phone;
            $patientArray['dob'] = $dob;
            $patientArray['deleted'] = 0;
            $patientArray['hospital_id'] = "";
            $patientArray['dept'] = $dept;
            $patientArray['division'] = $division;
            $patientArray['unit'] = $unit;
            $patientArray['qualification'] = $qualification;
            $patientArray['position'] = $position;
            $patientArray['registered_by'] = $registered_by;
            $patientArray['specialization'] = $specialization;
            $patientArray['hospital_id'] = "";
            $patientArray['staff_status'] = $staff_status;
            $patientArray['staff_category'] = $staff_category;
            $patientArray['email'] = $email;
            $patientArray['occupation'] = $occupation;
            $patientArray['mstatus'] = $mstatus;
            $patientArray['nospouse'] = $nospouse;
            $patientArray['nationality'] = $nationality;
            $patientArray['religion'] = $religion;
            $patientArray['salary_scale'] = $salary_scale;

            $file = request()->file('file');

            $fullname = $file->getClientOriginalName();

            $target_path = "uploads";

            $target_path1 = "/app/uploads";

            //$target_file1= storage_path().$target_path1. "/" . $patient_id.".png";

            $patientArray['picture_url'] = $staff_id.".png";


            Storage::putFileAs( $target_path, $file, $fullname );

            // save the patient
            $patient = Staff::create($patientArray);
            
           
      if($patient){
           
            return redirect()->route('patients')->with('success', "New staff successfully! ");
        }
        
        return redirect()->back()->with('error', "Error. Something went wrong, unable to create staff.");
        
  }
    catch(\Exception $e){
         //dd($e);
        return redirect()->back()->with('error', "Error. Something went wrong, unable to create staff. Try again or contact system administrator.");
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
