<?php

namespace App\Http\Controllers;

use App\Models\Schedule;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function getPatientSchedule($patientid){
        $schedule = Schedule::where(['patient_id'=>$patientid])->orderBy('date_registered', 'desc')->first();
        return view('admin.patients.entrydetails',compact('schedule'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $patient_id = $request->patient_id;
            $date_of_schedule = $request->date_of_schedule;
            $time_for_schedule = $request->time_for_schedule;
            $reason_of_schedule = $request->reason_of_schedule;
            $doctor_assigned = $request->doctor_assigned;
            $date_registered = Carbon::now();
            $appointment_status_id = $request->appointment_status_id;
            $schedule_type_id = $request->schedule_type_id;
            $patient_type_id = $request->patient_type_id;
            $appointment_status_note = $request->appointment_status_note;
            $email_remindal = $request->email_remindal;
            $phone_remindal = $request->phone_remindal;
            $refferal_detail = $request->refferal_detail;
            $registered_by = auth()->user()->id;
            $entry_id = uniqid();    
            
            $scheduleArray['patient_id'] = $patient_id;
            $scheduleArray['date_of_schedule'] = $date_of_schedule;
            $scheduleArray['time_for_schedule'] = $time_for_schedule;
            $scheduleArray['reason_of_schedule'] = $reason_of_schedule;
            $scheduleArray['doctor_assigned'] = $doctor_assigned;
            $scheduleArray['date_registered'] = $date_registered;
            $scheduleArray['appointment_status_id'] = $appointment_status_id;
            $scheduleArray['schedule_type_id'] = $schedule_type_id;
            $scheduleArray['patient_type_id'] = $patient_type_id;
            $scheduleArray['appointment_status_note'] = $appointment_status_note;
            $scheduleArray['email_remindal'] = $email_remindal;
            $scheduleArray['phone_remindal'] = $phone_remindal;
            $scheduleArray['refferal_detail'] = $refferal_detail;
            $scheduleArray['registered_by'] = $registered_by;
            $scheduleArray['entry_id'] = $entry_id;
            

            // save the schedule
            $schedule = Schedule::create($scheduleArray);
            
           
      if($schedule){
           
            return redirect()->route('view_patient')->with('success', "New schedule created successfully! ");
        }
        
        return redirect()->back()->with('error', "Error. Something went wrong, unable to create schedule.");
        
  }
    catch(\Exception $e){
         //dd($e);
        return redirect()->back()->with('error', "Error. Something went wrong, unable to create schedule. Try again or contact system administrator.");
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
