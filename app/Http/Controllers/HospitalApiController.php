<?php

namespace App\Http\Controllers;

use App\Models\PatientExam;
use App\Models\Patient;
use App\Models\Symptoms;
use Illuminate\Http\Request;
use DB;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;

class HospitalApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getDiagnosis()
    {

        $assess = PatientExam::all();

        $response = [
            'success' => true,
            "message" => "Loaded successfully",
            "data" => $assess
        ];
        return response()->json($response, 200);
    }



    public function get_symptoms()
    {
        $getsymptoms = Symptoms::all();
        $symptomCount = $getsymptoms->count();

        if($symptomCount < 1){

        $content = "";


        $timesammp = DATE("dmyHis");
        $orderID = $timesammp;

        $verb = "GET";

        $tokendetails = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6InJpY2hhcmQuZW1vY2hlQGdtYWlsLmNvbSIsInJvbGUiOiJVc2VyIiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvc2lkIjoiMTAwNTIiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3ZlcnNpb24iOiIyMDAiLCJodHRwOi8vZXhhbXBsZS5vcmcvY2xhaW1zL2xpbWl0IjoiOTk5OTk5OTk5IiwiaHR0cDovL2V4YW1wbGUub3JnL2NsYWltcy9tZW1iZXJzaGlwIjoiUHJlbWl1bSIsImh0dHA6Ly9leGFtcGxlLm9yZy9jbGFpbXMvbGFuZ3VhZ2UiOiJlbi1nYiIsImh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwOC8wNi9pZGVudGl0eS9jbGFpbXMvZXhwaXJhdGlvbiI6IjIwOTktMTItMzEiLCJodHRwOi8vZXhhbXBsZS5vcmcvY2xhaW1zL21lbWJlcnNoaXBzdGFydCI6IjIwMjEtMTItMTciLCJpc3MiOiJodHRwczovL3NhbmRib3gtYXV0aHNlcnZpY2UucHJpYWlkLmNoIiwiYXVkIjoiaHR0cHM6Ly9oZWFsdGhzZXJ2aWNlLnByaWFpZC5jaCIsImV4cCI6MTYzOTc2MjI3OSwibmJmIjoxNjM5NzU1MDc5fQ.ZS0tMJekl_iNQSk82H6Z8L4sgxVVfGNxhm1NcVAF2ns";

        $url = "https://sandbox-healthservice.priaid.ch/symptoms?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6InJpY2hhcmQuZW1vY2hlQGdtYWlsLmNvbSIsInJvbGUiOiJVc2VyIiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvc2lkIjoiMTAwNTIiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3ZlcnNpb24iOiIyMDAiLCJodHRwOi8vZXhhbXBsZS5vcmcvY2xhaW1zL2xpbWl0IjoiOTk5OTk5OTk5IiwiaHR0cDovL2V4YW1wbGUub3JnL2NsYWltcy9tZW1iZXJzaGlwIjoiUHJlbWl1bSIsImh0dHA6Ly9leGFtcGxlLm9yZy9jbGFpbXMvbGFuZ3VhZ2UiOiJlbi1nYiIsImh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwOC8wNi9pZGVudGl0eS9jbGFpbXMvZXhwaXJhdGlvbiI6IjIwOTktMTItMzEiLCJodHRwOi8vZXhhbXBsZS5vcmcvY2xhaW1zL21lbWJlcnNoaXBzdGFydCI6IjIwMjEtMTItMTciLCJpc3MiOiJodHRwczovL3NhbmRib3gtYXV0aHNlcnZpY2UucHJpYWlkLmNoIiwiYXVkIjoiaHR0cHM6Ly9oZWFsdGhzZXJ2aWNlLnByaWFpZC5jaCIsImV4cCI6MTYzOTkxODU0NSwibmJmIjoxNjM5OTExMzQ1fQ.-mDyuIrzCmwi7pY7hecAuY-nzOl6RRXXYjBlPDdU6eo&format=json&language=en-gb";

        $headers_new = array("Content-type: application/json");
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_CUSTOMREQUEST => $verb,
            CURLOPT_HTTPHEADER => $headers_new,
            CURLOPT_CONNECTTIMEOUT => 1200,
            CURLOPT_TIMEOUT        => 1200,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true
        ));
        $response = curl_exec($curl);
        // die(print_r($response));
        curl_close($curl);

        $response1 = json_decode($response, true);

        @$result = $response1;
        $i = 0;
        $datebatch = date('YmdHis');
        if (is_array(@$result)) {

            foreach (@$result as $dataArray) {
                $ID = $dataArray['ID'];
                $Name = $dataArray['Name'];

                @$result = DB::Statement("insert ignore into symptoms (`IDD`,`Name`) VALUES ( '" . $ID . "','" . $Name . "');");
            }
        }
    }

        $patients = Patient::all();
        return view('admin.patients.patients',compact('patients'));
        //return redirect()->back()->with("success", "successfully");
    }

    public function get_diagnosis($symptoms, $gender, $year_of_birth)
    {

        $content = "";

        //$orderID = $timesammp;

        $timesammp = DATE("dmyHis");
        $orderID = $timesammp;

        $verb = "GET";

        $url = "https://sandbox-healthservice.priaid.ch/diagnosis?symptoms=[$symptoms]&gender=$gender&year_of_birth=$year_of_birth&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6InJpY2hhcmQuZW1vY2hlQGdtYWlsLmNvbSIsInJvbGUiOiJVc2VyIiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvc2lkIjoiMTAwNTIiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3ZlcnNpb24iOiIyMDAiLCJodHRwOi8vZXhhbXBsZS5vcmcvY2xhaW1zL2xpbWl0IjoiOTk5OTk5OTk5IiwiaHR0cDovL2V4YW1wbGUub3JnL2NsYWltcy9tZW1iZXJzaGlwIjoiUHJlbWl1bSIsImh0dHA6Ly9leGFtcGxlLm9yZy9jbGFpbXMvbGFuZ3VhZ2UiOiJlbi1nYiIsImh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwOC8wNi9pZGVudGl0eS9jbGFpbXMvZXhwaXJhdGlvbiI6IjIwOTktMTItMzEiLCJodHRwOi8vZXhhbXBsZS5vcmcvY2xhaW1zL21lbWJlcnNoaXBzdGFydCI6IjIwMjEtMTItMTciLCJpc3MiOiJodHRwczovL3NhbmRib3gtYXV0aHNlcnZpY2UucHJpYWlkLmNoIiwiYXVkIjoiaHR0cHM6Ly9oZWFsdGhzZXJ2aWNlLnByaWFpZC5jaCIsImV4cCI6MTYzOTk0MDgyNSwibmJmIjoxNjM5OTMzNjI1fQ.Gh7g6lIlisS1PolVSqLnCl3GYeefzEKsCqEzxqyfQE4&format=json&language=en-gb";

        $headers_new = array("Content-type: application/json");

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_CUSTOMREQUEST => $verb,
            CURLOPT_HTTPHEADER => $headers_new,
            CURLOPT_CONNECTTIMEOUT => 1200,
            CURLOPT_TIMEOUT        => 1200,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true
        ));
        $response = curl_exec($curl);

        // die(print_r($response));
        curl_close($curl);

        $response1 = json_decode($response, true);

        @$result =  $response1;

        $i = 0;
        $datebatch = date('YmdHis');
        if (is_array(@$result)) {

            foreach (@$result as $key => $dataArray) {

                $Issueid=0;
                if (array_key_exists('Issue', $dataArray)) {


                    $ID = $dataArray['Issue']['ID'];
                    $Name = $dataArray['Issue']['Name'];
                    $Accuracy = $dataArray['Issue']['Accuracy'];
                    $Icd = $dataArray['Issue']['Icd'];
                    $IcdName = $dataArray['Issue']['IcdName'];
                    $ProfName = $dataArray['Issue']['ProfName'];
                    $Ranking = $dataArray['Issue']['Ranking'];
                    $Issueid = $dataArray['Issue']['ID'];



                    @$result = DB::Statement("insert ignore into diagnosis (`IDD`,`Name`,`Accuracy`,`Icd`,`IcdName`,`ProfName`,`Ranking`) VALUES ( '" . $ID . "','" . $Name . "','" . $Accuracy . "','" . $Icd . "','" . $IcdName . "','" . $ProfName . "','" . $Ranking . "');");
                }
                if (array_key_exists('Specialisation', $dataArray)) {

                    $arrlength = count($dataArray['Specialisation']);

                    for ($x = 0; $x < $arrlength; $x++) {

                        $IDs = $dataArray['Specialisation'][$x]['ID'];
                        $Names = $dataArray['Specialisation'][$x]['Name'];
                        $SpecialistIDs = $dataArray['Specialisation'][$x]['SpecialistID'];

                        @$result = DB::Statement("insert ignore into specializations (`IDD`,`Name`,`SpecialistID`,`IssueID`) VALUES ( '" . $IDs . "','" . $Names . "','" . $SpecialistIDs . "','" . $Issueid . "');");
                    }
                }
            }
        }

        return redirect()->back()->with("success", "successfully");
    }
}
