<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Models\Reader;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {

        $user =  User::create([
            'name' => $data['firstname'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'group_id' => 500,
        ]);

        $user_id = $user->id;

        $readerArray['lastname'] = $data['lastname'];
        $readerArray['firstname'] = $data['firstname'];
        $readerArray['uuid'] = uniqid();
        $readerArray['email'] = $data['email'];
        $readerArray['enabled'] = 1;
        $readerArray['deleted'] = 0;
        $readerArray['user_id'] = $user_id;
        
        // save the reader
        Reader::create($readerArray);

        return $user;

        /*if($done){
               
            return redirect()->back()->with('success',"Registration Successful");
        }
        
        return redirect()->back()->with('error', "Error. Something went wrong, unable to create book.");
        */

    } 
}
