<?php

namespace App\Http\Controllers;

use App\Models\EntryDetails;
use App\Models\VitalSigns;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

class EntryDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $details = EntryDetails::all();
        return view('admin.patients.entrydetails',compact('details'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            
            $registered_by = auth()->user()->id;
            $date_registered = Carbon::now();
            $patient_id = $request->patient_id;
            $entry_id = uniqid();
            $entry_type = $request->entry_type;
            
            $patient_age = $request->patient_age;
            $heart_rate_minutes = $request->heart_rate_minutes;
            $temperature = $request->temperature;
            $systolic_blood_pressure = $request->systolic_blood_pressure;
            $diastolic_blood_pressure = $request->diastolic_blood_pressure;
            $respiratory_rate = $request->respiratory_rate;
            //$date_expected = Carbon::parse('+10 days');
            
            $entryArray['registered_by'] = $registered_by;
            $entryArray['patient_id'] = $patient_id;
            $entryArray['date_registered'] = $date_registered;
            $entryArray['entry_id'] = $entry_id;
            $entryArray['entry_type'] = $entry_type;

            $vsignArray['registered_by'] = $registered_by;
            $vsignArray['patient_id'] = $patient_id;
            $vsignArray['date_registered'] = $date_registered;
            $vsignArray['entry_id'] = $entry_id;
            $vsignArray['patient_age'] = $patient_age;
            $vsignArray['heart_rate_minutes'] = $heart_rate_minutes;
            $vsignArray['temperature'] = $temperature;
            $vsignArray['systolic_blood_pressure'] = $systolic_blood_pressure;
            $vsignArray['diastolic_blood_pressure'] = $diastolic_blood_pressure;
            $vsignArray['respiratory_rate'] = $respiratory_rate;

            // save the vital signs
            $vsign = VitalSigns::create($vsignArray);

            // save the entry details
            $detail = EntryDetails::create($entryArray);
           
      if($detail){
           
            return redirect()->route('view_patient')->with('success', "Patient Checked in! ");
        }
        
        return redirect()->back()->with('error', "Error. Something went wrong, unable to create check in.");
        
  }
    catch(\Exception $e){
         //dd($e);
        return redirect()->back()->with('error', "Error. Something went wrong, unable to create check in. Try again or contact system administrator.");
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
