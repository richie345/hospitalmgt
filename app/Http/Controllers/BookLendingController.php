<?php

namespace App\Http\Controllers;

use App\Models\BookLending;
use App\Models\Book;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

class BookLendingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function borrowBook($id){

        try{
            

            $user_id = auth()->user()->id;
            $date_borrowed = Carbon::now();
            $date_expected = Carbon::parse('+10 days');
            $num_days_out = 10;
            
            $bookArray['book_id'] = $id;
            $bookArray['user_id'] = $user_id;
            $bookArray['date_borrowed'] = $date_borrowed;
            $bookArray['date_expected'] = $date_expected;
            $bookArray['num_of_days_out'] = $num_days_out;

            // save the book
            $book = BookLending::create($bookArray);

            $book = Book::where(['book_id'=>$id])->first();

            $update = Book::find($book->id);
            $update->borrowed = "yes";
            $update->save();
           
      if($book){
           
            return redirect()->route('available_books')->with('success', "Book Checked Out! ");
        }
        
        return redirect()->back()->with('error', "Error. Something went wrong, unable to create book.");
        
  }
    catch(\Exception $e){
         //dd($e);
        return redirect()->back()->with('error', "Error. Something went wrong, unable to create book. Try again or contact system administrator.");
    }
    }

    public function returnBook($id){

        try{
            

            $user_id = auth()->user()->id;
            
            // save the book
            $bookborrowed = BookLending::find($id);

            //$bookborrowed->save();

            $book = Book::where(['book_id'=>$bookborrowed->book_id])->first();
            
            $update = Book::find($book->id);
            $update->borrowed = "no";
            $update->save();
           
      if($book){
           
            return redirect()->route('available_books')->with('success', "Book Checked In! ");
        }
        
        return redirect()->back()->with('error', "Error. Something went wrong, unable to create book.");
        
  }
    catch(\Exception $e){
         //dd($e);
        return redirect()->back()->with('error', "Error. Something went wrong, unable to create book. Try again or contact system administrator.");
    }
    }

    public function myBorrowedBooks()
    {
        //$books = Book::where(['borrowed'=>"yes"])->get();
        $books = DB::table('book_lendings')
                     ->join('books', 'books.book_id', '=', 'book_lendings.book_id')
                     ->select('books.*','book_lendings.id','book_lendings.date_borrowed','book_lendings.user_id',
                              'book_lendings.date_expected','book_lendings.date_returned')
                     ->where('books.borrowed', '=', 'yes')
                     ->get();
        return view('admin.books.myborrowed_books',compact('books'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BookLending  $bookLending
     * @return \Illuminate\Http\Response
     */
    public function show(BookLending $bookLending)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BookLending  $bookLending
     * @return \Illuminate\Http\Response
     */
    public function edit(BookLending $bookLending)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BookLending  $bookLending
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BookLending $bookLending)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BookLending  $bookLending
     * @return \Illuminate\Http\Response
     */
    public function destroy(BookLending $bookLending)
    {
        //
    }
}
