<?php

namespace App\Http\Controllers;

use App\Models\Patient;
use App\Models\PatientExam;
use App\Models\Schedule;
use App\Models\ScheduleType;
use App\Models\Specializations;
use App\Models\VitalSigns;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Storage;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patients = Patient::all();
        return view('admin.patients.patients',compact('patients'));
    }

    public function addPatientView()
    {
        return view('admin.patients.add_patient');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $patient_id = $request->patient_id;
            $title_id = $request->title_id;
            $surname = $request->surname;
            $firstname = $request->firstname;
            $othername = $request->othername;
            $address = $request->address;
            $gender = $request->gender;
            $phone = $request->phone;
            $dob = $request->dob;
            $blood_group = $request->blood_group;
            $genotype = $request->genotype;
            $full_names_kin = $request->full_names_kin;
            $kin_phone = $request->kin_phone;
            //$kin_address = $request->kin_address;
            $registered_by = auth()->user()->id;
            $deathdate = $request->deathdate;
            //$patient_status = $request->patient_status;
            $patient_type = $request->patient_type;
            $email = $request->email;
            $occupation = $request->occupation;
            $mstatus = $request->mstatus;
            //$nospouse = $request->nospouse;
            $nationality = $request->nationality;
            $religion = $request->religion;
            $pob = $request->pob;
            $reffered_by = $request->reffered_by;
            
            $patientArray['patient_id'] = $patient_id;
            $patientArray['title_id'] = $title_id;
            //$patientArray['uuid'] = uniqid();
            $patientArray['surname'] = $surname;
            $patientArray['firstname'] = $firstname;
            $patientArray['othername'] = $othername;
            $patientArray['address'] = $address;
            $patientArray['gender'] = $gender;
            $patientArray['dateregistered'] = Carbon::now();
            $patientArray['phone'] = $phone;
            $patientArray['dob'] = $dob;
            $patientArray['deleted'] = 0;
            $patientArray['blood_group'] = $blood_group;
            $patientArray['genotype'] = $genotype;
            $patientArray['full_names_kin'] = $full_names_kin;
            $patientArray['kin_phone'] = $kin_phone;
            $patientArray['kin_address'] = "";
            $patientArray['registered_by'] = $registered_by;
            $patientArray['deathdate'] = $deathdate;
            $patientArray['hospital_id'] = "";
            $patientArray['patient_status'] = "active";
            $patientArray['patient_type'] = $patient_type;
            $patientArray['email'] = $email;
            $patientArray['occupation'] = $occupation;
            $patientArray['mstatus'] = $mstatus;
            $patientArray['nospouse'] = "";
            $patientArray['nationality'] = $nationality;
            $patientArray['religion'] = $religion;
            $patientArray['pob'] = $pob;
            $patientArray['reffered_by'] = $reffered_by;

            $file = request()->file('file');

            $fullname = $file->getClientOriginalName();

            $target_path = "uploads";

            $path = $file->storeAs('images', $patient_id.".png", 'public_uploads');


            $target_path1 = "/app/uploads";

            //$target_file1= storage_path().$target_path1. "/" . $patient_id.".png";

            $patientArray['picture_url'] = $patient_id.".png";


            //Storage::putFileAs( $target_path, $file, $patient_id );

            // save the patient
            $patient = Patient::create($patientArray);
            
           
      if($patient){
           
            return redirect()->route('patients')->with('success', "New Patient successfully! ");
        }
        
        return redirect()->back()->with('error', "Error. Something went wrong, unable to create Patient.");
        
  }
    catch(\Exception $e){
         //dd($e);
        return redirect()->back()->with('error', "Error. Something went wrong, unable to create patient. Try again or contact system administrator.");
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($patientid)
    {
        $patient = Patient::where(['patient_id'=>$patientid])->first();
        $scheduletypes = ScheduleType::all();
        $vsigns = VitalSigns::where(['patient_id'=>$patientid])->orderBy('date_registered', 'desc')->get();
        $schedules = Schedule::where(['patient_id'=>$patientid])->orderBy('date_registered', 'desc')->get();
        $doctors = DB::table('specializations')
                    ->select('id','IDD', 'Name')
                    ->groupBy('Name')
                    ->get();
        $examination = DB::table('patientexam')
                     ->join('symptoms', 'patientexam.presenting_complain', '=', 'symptoms.IDD')
                     ->select('patientexam.*','symptoms.Name')
                     ->orderBy('date_carried_out', 'desc')
                     ->get();
        
        return view('admin.patients.viewPatient',compact('patient', 'scheduletypes', 'schedules', 'vsigns', 'examination', 'doctors'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
