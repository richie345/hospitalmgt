<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    use HasFactory;
    protected $table = "staff";

    protected $fillable = [
        'staff_id','title_id','surname','firstname','othername','address','gender','phone','dob',
        'dept','division','unit','qualification','position','dateregistered','registered_by',
        'specialization','hospital_id','picture_url','staff_status','email','signature',
        'mstatus','nospouse','nationality','religion','staff_category','salary_scale','deleted'
    ];

}
