<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EntryDetails extends Model
{
    use HasFactory;
    protected $table = "entrydetails";

    protected $fillable = [
        'entry_id','patient_id','entry_type','date_registered','registered_by'
    ];

}
