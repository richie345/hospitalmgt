<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PatientExam extends Model
{
    use HasFactory;
    protected $table = "patientexam";

    protected $fillable = [
        'title','patient_id','presenting_complain','history_of_present_illness','medications',
        'allergies','past_medical_history','family_history','social_history','drug_history',
        'occupational_history','review_of_systems','physical_mental_examination','lab','assessment',
        'carried_out_by','plan','entry_id','date_carried_out','remarks','valid'
    ];

}
