<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    use HasFactory;

    protected $table = "patient";

    protected $fillable = [
        'patient_id','title_id','surname','firstname','othername','address','gender','phone','dob',
        'blood_group','genotype','full_names_kin','kin_phone','kin_address','dateregistered','registered_by',
        'deathdate','hospital_id','picture_url','patient_status','patient_type','email','occupation',
        'mstatus','nospouse','nationality','religion','pob','reffered_by','deleted'
    ];

}
