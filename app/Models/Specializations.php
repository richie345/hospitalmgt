<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Specializations extends Model
{
    use HasFactory;
    protected $table = "specializations";
    
    protected $fillable = [
        'IDD','Name','SpecialistID','IssueID'
    ];

}
