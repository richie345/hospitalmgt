<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    use HasFactory;
    protected $table = "schedule";

    protected $fillable = [
        'entry_id','patient_id','date_of_schedule','time_for_schedule','reason_of_schedule','doctor_assigned',
        'date_registered','appointment_status_id','schedule_type_id','patient_type_id','appointment_status_note',
        'registered_by','schedule_status','email_remindal','phone_remindal','refferal_detail'
    ];

}
