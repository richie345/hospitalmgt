<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScheduleType extends Model
{
    use HasFactory;
    protected $table = "scheduletype";

    protected $fillable = [
        'schedule_type','lab_item','imaging_item','surgery_item'
    ];

}
