<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VitalSigns extends Model
{
    use HasFactory;
    protected $table = "vitalsigns";

    protected $fillable = [
        'entry_id','patient_id','patient_age','heart_rate_minutes','temperature',
        'systolic_blood_pressure','diastolic_blood_pressure','respiratory_rate','date_registered',
        'registered_by'
    ];

}
