<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVitalsignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vitalsigns', function (Blueprint $table) {
            $table->id();
            $table->string('patient_id');
            $table->string('entry_id');
            $table->integer('patient_age')->nullable();
            $table->string('heart_rate_minutes');
            $table->string('temperature')->nullable();
            $table->string('systolic_blood_pressure')->nullable();
            $table->string('diastolic_blood_pressure')->nullable();
            $table->string('respiratory_rate')->nullable();
            $table->string('date_registered')->nullable();
            $table->string('registered_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vitalsigns');
    }
}
