<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientexamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patientexam', function (Blueprint $table) {
            $table->id();
            $table->string('patient_id');
            $table->string('title')->nullable();
            $table->string('entry_id')->nullable();
            $table->string('presenting_complain');
            $table->string('history_of_present_illness');
            $table->string('medications')->nullable();
            $table->string('allergies')->nullable();
            $table->string('past_medical_history')->nullable();
            $table->string('family_history')->nullable();
            $table->string('social_history')->nullable();
            $table->string('drug_history')->nullable();
            $table->string('occupational_history')->nullable();
            $table->string('review_of_systems')->nullable();
            $table->string('physical_mental_examination')->nullable();
            $table->string('lab')->nullable();
            $table->string('assessment')->nullable();
            $table->string('carried_out_by')->nullable();
            $table->string('plan')->nullable();
            $table->string('date_carried_out')->nullable();
            $table->string('remarks')->nullable();
            $table->string('valid')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patientexam');
    }
}
