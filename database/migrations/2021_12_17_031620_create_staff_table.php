<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff', function (Blueprint $table) {
            $table->id();
            $table->string('staff_id')->unique();
            $table->string('title_id')->nullable();
            $table->string('surname');
            $table->string('firstname');
            $table->string('othername')->nullable();
            $table->string('address')->nullable();
            $table->string('picture_url')->nullable();
            $table->string('phone')->nullable();
            $table->string('dob')->nullable();
            $table->string('dept')->nullable();
            $table->string('division')->nullable();
            $table->string('unit')->nullable();
            $table->string('qualification')->nullable();
            $table->string('position')->nullable();
            $table->string('specialization')->nullable();
            $table->string('dateregistered')->nullable();
            $table->string('registered_by')->nullable();
            $table->string('staff_status')->nullable();
            $table->string('hospital_id')->nullable();
            $table->string('signature')->nullable();
            $table->string('mstatus')->nullable();
            $table->string('nospouse')->nullable();
            $table->string('nationality')->nullable();
            $table->string('religion')->nullable();
            $table->string('staff_category')->nullable();
            $table->string('salary_scale')->nullable();
            $table->string('gender')->nullable();
            $table->string('deleted')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff');
    }
}
