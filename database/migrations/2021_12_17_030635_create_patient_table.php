<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient', function (Blueprint $table) {
            $table->id();
            $table->string('patient_id')->unique();
            $table->string('title_id')->nullable();
            $table->string('surname');
            $table->string('firstname');
            $table->string('othername')->nullable();
            $table->string('address')->nullable();
            $table->string('picture_url')->nullable();
            $table->string('phone')->nullable();
            $table->string('dob')->nullable();
            $table->string('blood_group')->nullable();
            $table->string('genotype')->nullable();
            $table->string('full_names_kin')->nullable();
            $table->string('kin_phone')->nullable();
            $table->string('email')->nullable();
            $table->string('kin_address')->nullable();
            $table->string('dateregistered')->nullable();
            $table->string('registered_by')->nullable();
            $table->string('deathdate')->nullable();
            $table->string('hospital_id')->nullable();
            $table->string('patient_status')->nullable();
            $table->string('patient_type')->nullable();
            $table->string('occupation')->nullable();
            $table->string('mstatus')->nullable();
            $table->string('nospouse')->nullable();
            $table->string('nationality')->nullable();
            $table->string('religion')->nullable();
            $table->string('pob')->nullable();
            $table->string('reffered_by')->nullable();
            $table->string('gender')->nullable();
            $table->string('deleted')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient');
    }
}
