<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule', function (Blueprint $table) {
            $table->id();
            $table->string('patient_id');
            $table->string('entry_id');
            $table->string('date_of_schedule');
            $table->string('time_for_schedule');
            $table->string('reason_of_schedule');
            $table->string('doctor_assigned')->nullable();
            $table->string('date_registered')->nullable();
            $table->string('appointment_status_id')->nullable();
            $table->string('schedule_type_id')->nullable();
            $table->string('patient_type_id')->nullable();
            $table->string('appointment_status_note')->nullable();
            $table->string('registered_by')->nullable();
            $table->string('email_remindal')->nullable();
            $table->string('phone_remindal')->nullable();
            $table->string('schedule_status')->nullable();
            $table->string('refferal_detail')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedule');
    }
}
