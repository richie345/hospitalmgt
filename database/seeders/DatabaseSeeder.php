<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        DB::table("users")->InsertOrIgnore([
         [
         'name'=>'Admin',
         'email'=>'admin@test.com',
         'password'=>'$2y$10$6SmVomdt4LQC.u0IQAbW1OmdWYSHWaP/37ELb6OA2.JwLQEGH7zC6'
         ]
         
        ]);
    }
}
