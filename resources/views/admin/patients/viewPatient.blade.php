@extends('layouts.admin.master')

@section('title')Hospital Management
 View Patient
@endsection

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/datatables.css') }}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/animate.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/css/chartist.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/css/date-picker.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/css/prism.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/css/vector-map.css')}}">
@endpush

@section('content')
	@component('components.breadcrumb')
		@slot('breadcrumb_title')
			<h3>Patient History</h3>
		@endslot
		<li class="breadcrumb-item">Patient</li>
		<li class="breadcrumb-item">{{ $patient->firstname }} {{ $patient->surname }}</li>
	@endcomponent
	
	<div class="container-fluid dashboard-default-sec">
        <div class="row">
          <div class="col-xl-5 box-col-12 des-xl-100">
            <div class="row">
              <div class="col-xl-6 col-md-3 box-col-3 des-xl-25">
                <div class="card profile-greeting2">
                  <div class="card-header">
                    <div class="header-top">
                      <div class="setting-list bg-primary position-unset">
                        
                      </div>
                    </div>
                  </div>
                  <div class="card-body text-center p-t-0">
                    <img src="{{ asset('uploads/images/'.$patient->picture_url) }}" height="150" width="150">
                    
                  </div>
                  
                </div>
              </div>
              <div class="col-xl-6 col-md-3 col-sm-6 box-col-3 des-xl-25 rate-sec">
                <div class="card income-card card-primary">
                  <div class="card-body text-center">
                    <div class="round-box">
                      
                    </div>
                    <h5 class="font-light"> {{ $patient->surname }} {{ $patient->firstname }} {{ $patient->othername }}</h5>
                    <p>{{ $patient->patient_id }}</p>
                    <div class="parrten">
                     
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-6 col-md-3 col-sm-6 box-col-3 des-xl-25 rate-sec">
                <div class="card income-card card-secondary">
                  <div class="card-body text-center">
                    <div class="round-box">
                     
                    </div>
                    <h5 class="font-light"> {{ $patient->gender }}</h5>
                    <p>{{ $patient->dob }}</p>
                    <div class="parrten">
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      
          <div class="col-xl-8 box-col-12 des-xl-100">
            <div class="row">
              <div class="col-xl-6 col-50 box-col-6 des-xl-50">
                <div class="card">
                  <div class="card-header">
                    <div class="header-top d-sm-flex align-items-center">
                      <h5>Demographics</h5>
                      
                    </div>
                  </div>
                  <div class="card-body p-0">
                  <div class="table-responsive">
                      <table class="table table-bordernone">
                        <tbody>
                          <tr>
                            <td>
                              <div class="media">
                                <div class="media-body"><span>Fullname</span>
                                </div>
                              </div>
                            </td>
                            <td>{{ $patient->surname }} {{ $patient->firstname }} {{ $patient->othername }}</td>
                          </tr>
                          <tr>
                            <td>
                              <div class="media">
                                
                                <div class="media-body"><span>Address</span>
                                </div>
                              </div>
                            </td>
                            <td>{{ $patient->address }}</td>
                          </tr>
                          <tr>
                            <td>
                              <div class="media">
                                
                                <div class="media-body"><span>Phone</span>
                                </div>
                              </div>
                            </td>
                            <td>{{ $patient->phone }}</td>
                          </tr>
                          <tr>
                            <td>
                              <div class="media">
                                
                                <div class="media-body"><span>Email</span>
                                </div>
                              </div>
                            </td>
                            <td>{{ $patient->email }}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    
                  </div>
                </div>
              </div>
              <div class="col-xl-6 col-50 box-col-6 des-xl-50">
                <div class="card latest-update-sec">
                  <div class="card-header">
                    <div class="header-top d-sm-flex align-items-center">
                      <h5>Upcoming Appointments</h5>
                      <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModalmdo">Schedule Consultation</a> |
                      <a href="#" data-bs-toggle="modal" data-bs-target="#addcheckin">Add Checkin</a>
                    </div>
                  </div>
                  <div class="card-body p-0">
                    <div class="table-responsive">
                      <table class="table table-bordernone">
                        <tbody>
                        @foreach($schedules as $sch)
                          <tr>
                            <td>
                              <span><a href="{{ url('/add_patient_exam', [$patient->patient_id, $sch->id]) }}">{{ $sch->reason_of_schedule }}</a></span>
                               
                            </td>
                            <td>{{ $sch->date_of_schedule }}{{ $sch->time_for_schedule }}</td>                         </a></td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                    
                  </div>
                </div>
              </div>
              <div class="col-xl-12 recent-order-sec">
                <div class="card">
                  <div class="card-body">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item"><a class="nav-link active" id="home-tab" data-bs-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Vital Signs</a></li>
                            <li class="nav-item"><a class="nav-link" id="profile-tabs" data-bs-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Consultations</a></li>
                            <li class="nav-item"><a class="nav-link" id="contact-tab" data-bs-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Diagnosis</a></li>
                            <li class="nav-item"><a class="nav-link" id="medications-tab" data-bs-toggle="tab" href="#medications" role="tab" aria-controls="medications" aria-selected="false">Medications</a></li>
                            <li class="nav-item"><a class="nav-link" id="allergies-tab" data-bs-toggle="tab" href="#allergies" role="tab" aria-controls="allergies" aria-selected="false">Allergies</a></li>
                            <li class="nav-item"><a class="nav-link" id="documents-tab" data-bs-toggle="tab" href="#documents" role="tab" aria-controls="documents" aria-selected="false">Documents</a></li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <p class="mb-0">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Age</th>
                                                <th>Heart Rate</th>
                                                <th>Temp</th>
                                                <th>Systolic BP</th>
                                                <th>Diastolic BP</th>
                                                <th>Resp. Rate</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($vsigns as $v)
                                        <tr>
                                            <th scope="row"> {{ $v->patient_age }} </td>
                                            <td> {{ $v->heart_rate_minutes }} </td>
                                            <td> {{ $v->temperature }} </td>
                                            <td> {{ $v->systolic_blood_pressure }} </td>
                                            <td> {{ $v->diastolic_blood_pressure }} </td>
                                            <td> {{ $v->respiratory_rate }} </td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </p>
                            </div>
                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Reason</th>
                                                <th>Doctor</th>
                                                <th>Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($schedules as $s)
                                        <tr>
                                            <th scope="row"> {{ $s->reason_of_schedule }} </td>
                                            <td> {{ $s->doctor_assigned }} </td>
                                            <td> {{ $s->date_of_schedule }}{{ $s->time_for_schedule }} </td>
                                            
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                <p class="mb-0 m-t-30">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Complaint</th>
                                                <th>History</th>
                                                <th>Occupation</th>
                                                <th>Diagnosis</th>
                                                <th>Validity</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($examination as $exam)
                                        <tr>
                                            <th scope="row"> {{ $exam->title }} </td>
                                            <td> {{ $exam->Name }} </td>
                                            <td> {{ $exam->history_of_present_illness }} </td>
                                            <td> {{ $exam->occupational_history }} </td>
                                            <td> {{ $exam->remarks }} </td>
                                            <td> {{ $exam->valid }} <a href="{{ url('/change_validity', $exam->id)}}">Change</a></td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </p>
                            </div>
                            <div class="tab-pane fade" id="medications" role="tabpanel" aria-labelledby="medications-tab">
                                <p class="mb-0 m-t-30">
                                    test medications
                                </p>
                            </div>
                            <div class="tab-pane fade" id="allergies" role="tabpanel" aria-labelledby="allergies-tab">
                                <p class="mb-0 m-t-30">
                                    test allergies
                                </p>
                            </div>
                            <div class="tab-pane fade" id="documents" role="tabpanel" aria-labelledby="documents-tab">
                                <p class="mb-0 m-t-30">
                                    Scanned documents
                                </p>
                            </div>
                        </div>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-4 box-col-12 des-xl-100">
            
          </div>

        <div class="modal fade" id="exampleModalmdo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" style="overflow-y: initial !important" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Create Schedule</h5>
                        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form method="post" action="{{ route('create_schedule') }}">
                        @csrf
                        <div class="modal-body" style="height: 80vh; overflow-y: auto;" >
                            
                            <div class="mb-3">
                                <label class="col-form-label" for="recipient-name">Patient:</label>
                                <input class="form-control" name="patient-name" type="text" readonly value="{{ $patient->surname }} {{ $patient->firstname }}">
                                <input name="patient_id" type="hidden" value="{{ $patient->patient_id }}">
                            </div>
                            <div class="mb-3">
                                <label class="col-form-label" for="message-text">Reason for Schedule:</label>
                                <input class="form-control" name="reason_of_schedule" type="text" >
                            </div>
                            <div class="mb-3">
                                <label class="col-form-label" for="message-text">Schedule Type:</label>
                                <select class="form-control" name="scheduletype">
                                <option value="">-- Select Option --</option>
                                @foreach($scheduletypes as $stype)
                                    <option value="{{ $stype->id  }}">{{ $stype->schedule_type  }}</option>
                                @endforeach
                                </select>
                            </div>
                            <div class="mb-3">
                                <label class="col-form-label" for="message-text">Appointment Date:</label>
                                <input class="form-control" name="date_of_schedule" type="date" >
                            </div>
                            <div class="mb-3">
                                <label class="col-form-label" for="message-text">Appointment Time:</label>
                                <input class="form-control" name="time_for_schedule" type="time" >
                            </div>
                            <div class="mb-3">
                                <label class="col-form-label" for="message-text">Doctor Assigned:</label>
                                <select class="form-control" name="doctor_assigned">
                                    <option value="">-- Select Option --</option>
                                    @foreach($doctors as $dc)
                                        <option value="{{ $dc->id  }}">{{ $dc->Name  }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mb-3">
                                <label class="col-form-label" for="message-text">Referred By:</label>
                                <input class="form-control" name="refferal_detail" type="text" >
                            </div>
                            <div class="mb-3">
                                <label class="col-form-label" for="message-text">Send Reminder:</label>
                                Email: <input class="form-control" name="email_remindal" type="checkbox" value="yes" >
                                Phone: <input class="form-control" name="phone_remindal" type="checkbox" value="yes" >
                            </div>
                                
                        </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button>
                        <button class="btn btn-primary" name="save1" type="submit">Save changes</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="addcheckin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Check In Editor</h5>
                        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form method="post" action="{{ route('add_checkin') }}">
                        @csrf
                        <div class="modal-body">
                            
                                <div class="mb-3">
                                    <label class="col-form-label" for="recipient-name">Patient Name:</label>
                                    <input class="form-control" name="patient-name" type="text" readonly value="{{ $patient->surname }} {{ $patient->firstname }}">
                                    <input name="patient_id" type="hidden" value="{{ $patient->patient_id }}">
                                </div>
                                <div class="mb-3">
                                    <label class="col-form-label" for="message-text">Entry Type:</label>
                                    <select class="form-control" name="entrytype">
                                        <option>Check in</option>
                                        <option>Admitted</option>
                                        <option>Cancelled</option>
                                        <option>No Show</option>
                                    </select>
                                </div>
                                <hr>
                                <h5>Vital Signs</h5>
                                <div class="mb-3">
                                    <label class="col-form-label" for="message-text">Patient Age:</label>
                                    <input class="form-control" name="patient_age" type="number" >
                                </div>
                                <div class="mb-3">
                                    <label class="col-form-label" for="message-text">Heart Rate:</label>
                                    <input class="form-control" name="heart_rate_minutes" type="text" >
                                </div>
                                <div class="mb-3">
                                    <label class="col-form-label" for="message-text">Temperature:</label>
                                    <input class="form-control" name="temperature" type="text" >
                                </div>
                                <div class="mb-3">
                                    <label class="col-form-label" for="message-text">Systolic Blood Pressure:</label>
                                    <input class="form-control" name="systolic_blood_pressure" type="text" >
                                </div>
                                <div class="mb-3">
                                    <label class="col-form-label" for="message-text">Diastolic Blood Pressure:</label>
                                    <input class="form-control" name="diastolic_blood_pressure" type="text" >
                                </div>
                                <div class="mb-3">
                                    <label class="col-form-label" for="message-text">Respiratory Rate:</label>
                                    <input class="form-control" name="respiratory_rate" type="text" >
                                </div>
                                
                            
                        </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button>
                        <button class="btn btn-primary" type="submit">Save changes</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>

        </div>
      </div>

	
	@push('scripts')
    <script src="{{asset('assets/js/chart/chartist/chartist.js')}}"></script>
    <script src="{{asset('assets/js/chart/chartist/chartist-plugin-tooltip.js')}}"></script>
    <script src="{{asset('assets/js/chart/knob/knob.min.js')}}"></script>
    <script src="{{asset('assets/js/chart/knob/knob-chart.js')}}"></script>
    <script src="{{asset('assets/js/chart/apex-chart/apex-chart.js')}}"></script>
    <script src="{{asset('assets/js/chart/apex-chart/stock-prices.js')}}"></script>
    <script src="{{asset('assets/js/prism/prism.min.js')}}"></script>
    <script src="{{asset('assets/js/clipboard/clipboard.min.js')}}"></script>
    <script src="{{asset('assets/js/counter/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('assets/js/counter/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('assets/js/counter/counter-custom.js')}}"></script>
    <script src="{{asset('assets/js/custom-card/custom-card.js')}}"></script>
    <script src="{{asset('assets/js/notify/bootstrap-notify.min.js')}}"></script>
    <script src="{{asset('assets/js/vector-map/jquery-jvectormap-2.0.2.min.js')}}"></script>
    <script src="{{asset('assets/js/vector-map/map/jquery-jvectormap-world-mill-en.js')}}"></script>
    <script src="{{asset('assets/js/vector-map/map/jquery-jvectormap-us-aea-en.js')}}"></script>
    <script src="{{asset('assets/js/vector-map/map/jquery-jvectormap-uk-mill-en.js')}}"></script>
    <script src="{{asset('assets/js/vector-map/map/jquery-jvectormap-au-mill.js')}}"></script>
    <script src="{{asset('assets/js/vector-map/map/jquery-jvectormap-chicago-mill-en.js')}}"></script>
    <script src="{{asset('assets/js/vector-map/map/jquery-jvectormap-in-mill.js')}}"></script>
    <script src="{{asset('assets/js/vector-map/map/jquery-jvectormap-asia-mill.js')}}"></script>
    <script src="{{asset('assets/js/dashboard/default.js')}}"></script>
    <script src="{{asset('assets/js/notify/index.js')}}"></script>
    <script src="{{asset('assets/js/datepicker/date-picker/datepicker.js')}}"></script>
    <script src="{{asset('assets/js/datepicker/date-picker/datepicker.en.js')}}"></script>
    <script src="{{asset('assets/js/datepicker/date-picker/datepicker.custom.js')}}"></script>
	<script src="{{ asset('assets/js/datatable/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatables/datatable.custom.js') }}"></script>
    <script src="{{ asset('assets/js/tooltip-init.js')}}"></script>
	@endpush

@endsection