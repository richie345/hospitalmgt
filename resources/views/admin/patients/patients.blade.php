@extends('layouts.admin.master')

@section('title')Hospital Management
 View Patients
@endsection

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/datatables.css') }}">
@endpush

@section('content')
	@component('components.breadcrumb')
		@slot('breadcrumb_title')
			<h3>View All Patients in Hospital</h3>
		@endslot
		<li class="breadcrumb-item">Patients</li>
		<li class="breadcrumb-item">Registered Patients</li>
	@endcomponent
	
	<div class="container-fluid">
	    <div class="row">
	        <!-- Zero Configuration  Starts-->
	        <div class="col-sm-12">
	            <div class="card">
	               
	                <div class="card-body">
	                    <div class="table-responsive">
	                        <table class="display" id="basic-1">
	                            <thead>
	                                <tr>
                                        <th>#</th>
	                                    <th>Patient ID</th>
	                                    <th>Name</th>
	                                    <th>Gender</th>
	                                    <th>Phone No</th>
	                                    <th>DoB</th>
                                        <th>Type</th>
                                        <th>[Actions]</th>
	                                </tr>
	                            </thead>
	                            <tbody>
                                @foreach($patients as $data)
	                                <tr>
                                        <th>{{ $loop->iteration }}</th>
	                                    <td>{{ $data->patient_id }}</td>
	                                    <td>{{ $data->surname }} {{ $data->firstname }} {{ $data->othername }}</td>
	                                    <td>{{ $data->gender }}</td>
	                                    <td>{{ $data->phone }}</td>
	                                    <td>{{ $data->dob }} </td>
	                                    <td>{{ $data->patient_type }}</td>
                                        <td><a href="{{ url('/view_patient', $data->patient_id)}}" title="view patient"><i class="fa fa-search" aria-hidden="true"></i></a></td>
	                                </tr>
	                            @endforeach 
	                            </tbody>
	                        </table>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <!-- Zero Configuration  Ends-->
	     
	    </div>
	</div>

	
	@push('scripts')
	<script src="{{ asset('assets/js/datatable/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatables/datatable.custom.js') }}"></script>
	@endpush

@endsection