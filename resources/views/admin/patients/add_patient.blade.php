@extends('layouts.admin.master')

@section('title')Hospital Mgt
 Add Patients
@endsection

@push('css')
@endpush

@section('content')
	@component('components.breadcrumb')
		@slot('breadcrumb_title')
			<h3>Add New Patient</h3>
		@endslot
		<li class="breadcrumb-item">Patients</li>
		<li class="breadcrumb-item">New Patient</li>
	@endcomponent
	
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				
				<div class="card">
					<div class="card-header pb-0">
						<h5>New Patient</h5>
					</div>
					<form class="form theme-form" method="post" action="{{ route('add_patient') }}" enctype="multipart/form-data">
                        @csrf
						<div class="card-body">
							<div class="row">
								<div class="col">
									<div class="mb-3 row">
										<label class="col-sm-3 col-form-label">Title<span>*</span></label>
										<div class="col-sm-3">
											<select class="form-control" name="title_id" required>
                                                <option>Prof</option>
                                                <option>Mr</option>
                                                <option>Mrs</option>
												<option>Dr</option>
												<option>Chief</option>
												<option>Miss</option>
                                            </select>
										</div>
										<label class="col-sm-3 col-form-label">Patient ID<span>*</span></label>
										<div class="col-sm-3">
											<input class="form-control" name="patient_id" type="text" required />
										</div>
									</div>
									<div class="mb-3 row">
										<label class="col-sm-3 col-form-label">Surname</label>
										<div class="col-sm-9">
											<input class="form-control" name="surname" type="text" />
										</div>
									</div>
                                    <div class="mb-3 row">
										<label class="col-sm-3 col-form-label">First Name</label>
										<div class="col-sm-3">
											<input class="form-control" name="firstname" type="text" />
										</div>
										<label class="col-sm-3 col-form-label">Othernames</label>
										<div class="col-sm-3">
											<input class="form-control" name="othername" type="text" />
										</div>
									</div>
									<div class="mb-3 row">
										<label class="col-sm-3 col-form-label">Phone</label>
										<div class="col-sm-3">
											<input class="form-control" name="phone" type="text" placeholder="" />
										</div>
										<label class="col-sm-3 col-form-label">Email</label>
										<div class="col-sm-3">
											<input class="form-control" name="email" type="email" placeholder="" />
										</div>
									</div>
									<div class="mb-3 row">
										<label class="col-sm-3 col-form-label">Date of Birth</label>
										<div class="col-sm-3">
											<input class="form-control digits" name="dob" type="date" value="2018-01-01" />
										</div>
										<label class="col-sm-3 col-form-label">Gender</label>
										<div class="col-sm-3">
											<select class="form-control" name="gender">
                                                <option>Male</option>
                                                <option>Female</option>
                                                <option>Others</option>
                                            </select>
										</div>
									</div>
									<div class="mb-3 row">
										<label class="col-sm-3 col-form-label">Date of Death</label>
										<div class="col-sm-3">
											<input class="form-control digits" name="deathdate" type="date" value="2018-01-01" />
										</div>
										<label class="col-sm-3 col-form-label">Patient Type</label>
										<div class="col-sm-3">
											<select class="form-control" name="patient_type">
                                                <option value="inpatient">In Patient</option>
                                                <option value="outpatient">Out Patient</option>
                                            </select>
										</div>
									</div>
									<div class="mb-3 row">
										<label class="col-sm-3 col-form-label">Blood Group</label>
										<div class="col-sm-3">
											<select class="form-control" name="blood_group">
                                                <option>AB</option>
                                                <option>B+</option>
                                                <option>O+</option>
                                            </select>
										</div>
										<label class="col-sm-3 col-form-label">Genotype</label>
										<div class="col-sm-3">
											<select class="form-control" name="genotype">
                                                <option>AA</option>
                                                <option>AS</option>
                                                <option>SS</option>
                                            </select>
										</div>
									</div>
									<div class="mb-3 row">
										<label class="col-sm-3 col-form-label">Occupation</label>
										<div class="col-sm-3">
											<select class="form-control" name="occupation">
                                                <option>Civil Servant</option>
                                                <option>Private Sector</option>
                                            </select>
										</div>
										<label class="col-sm-3 col-form-label">Marital Status</label>
										<div class="col-sm-3">
											<select class="form-control" name="mstatus">
                                                <option>Single</option>
                                                <option>Married</option>
                                                <option>Divorced</option>
                                            </select>
										</div>
									</div>
                                    <div class="mb-3 row">
										<label class="col-sm-3 col-form-label">Nationality</label>
										<div class="col-sm-3">
											<select class="form-control" name="nationality">
                                                <option>Nigeria</option>
                                                <option>Others</option>
                                            </select>
										</div>
										<label class="col-sm-3 col-form-label">Religion</label>
										<div class="col-sm-3">
											<select class="form-control" name="religion">
                                                <option>Christianity</option>
                                                <option>Islam</option>
                                                <option>Others</option>
                                            </select>
										</div>
									</div>
									<div class="mb-3 row">
										<label class="col-sm-3 col-form-label">Place of Birth</label>
										<div class="col-sm-9">
											<input class="form-control" name="pob" type="text" />
										</div>
									</div>
									<div class="mb-3 row">
										<label class="col-sm-3 col-form-label">Reffered by</label>
										<div class="col-sm-9">
											<input class="form-control" name="reffered_by" type="text" placeholder=" Name" />
										</div>
									</div>
                                    <div class="mb-3 row">
										<label class="col-sm-3 col-form-label">Name of Next of kin</label>
										<div class="col-sm-3">
											<input class="form-control" name="full_names_kin" type="text" placeholder=" Name" />
										</div>
										<label class="col-sm-3 col-form-label">Next of kin Phone</label>
										<div class="col-sm-3">
											<input class="form-control" name="kin_phone" type="text" placeholder=" Phone" />
										</div>
									</div>
									<div class="mb-3 row">
										<label class="col-sm-3 col-form-label">Patient Image</label>
										<div class="col-sm-9">
											<input class="form-control" name="file" type="file" />
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card-footer text-end">
							<div class="col-sm-9 offset-sm-3">
								<button class="btn btn-primary" type="submit">Save Details</button>
								<input class="btn btn-light" type="reset" value="Cancel" />
							</div>
						</div>
					</form>
				</div>
			
			</div>
		</div>
	</div>
	
	
	@push('scripts')
	@endpush

@endsection