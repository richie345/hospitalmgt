@extends('layouts.admin.master')

@section('title')Hospital Mgt
 Add Patients
@endsection

@push('css')
@endpush

@section('content')
	@component('components.breadcrumb')
		@slot('breadcrumb_title')
			<h3>Patient Examination</h3>
		@endslot
		<li class="breadcrumb-item">Patient</li>
		<li class="breadcrumb-item">Patient Examination</li>
	@endcomponent
	
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				
				<div class="card">
					<div class="card-header pb-0">
						<h5>{{ $patient->title_id }} {{ $patient->firstname }} {{ $patient->othername }} {{ $patient->surname }}</h5>
					</div>
					<form class="form theme-form" method="post" action="{{ route('addpatientexam') }}" enctype="multipart/form-data">
                        @csrf
						<div class="card-body">
							<div class="row">
								<div class="col">
									
									<div class="mb-3 row">
										<label class="col-sm-3 col-form-label">Title/Topic</label>
										<div class="col-sm-9">
											<input class="form-control" name="title" type="text" />
                                            <input class="form-control" name="patient_id" type="hidden" value="{{ $patient->patient_id }}" />
										</div>
									</div>
                                    <div class="mb-3 row">
										<label class="col-sm-3 col-form-label">Complaint</label>
										<div class="col-sm-9">
                                            <select class="form-control" name="presenting_complain">
                                            <option value="">--Select One--</option>
                                                @foreach($symptoms as $data)
                                                    <option value="{{ $data->IDD }}">{{ $data->Name }}</option>
                                                @endforeach
                                            </select>
										</div>
									</div>
									<div class="mb-3 row">
										<label class="col-sm-3 col-form-label">History of Illness</label>
										<div class="col-sm-9">
											<input class="form-control" name="history_of_present_illness" type="text" />
										</div>
									</div>
									<div class="mb-3 row">
										<label class="col-sm-3 col-form-label">Past Medical History</label>
										<div class="col-sm-9">
											<input class="form-control" name="past_medical_history" type="text" />
										</div>
									</div>
									<div class="mb-3 row">
										<label class="col-sm-3 col-form-label">Allergies</label>
										<div class="col-sm-9">
											<input class="form-control" name="allergies" type="text" />
										</div>
									</div>
									<div class="mb-3 row">
										<label class="col-sm-3 col-form-label">Family History</label>
										<div class="col-sm-9">
											<input class="form-control" name="family_history" type="text" />
										</div>
									</div>
                                    <div class="mb-3 row">
										<label class="col-sm-3 col-form-label">Social History</label>
										<div class="col-sm-9">
											<input class="form-control" name="social_history" type="text" />
										</div>
									</div>
                                    <div class="mb-3 row">
										<label class="col-sm-3 col-form-label">Drug History</label>
										<div class="col-sm-9">
											<input class="form-control" name="drug_history" type="text" />
										</div>
									</div>
                                    <div class="mb-3 row">
										<label class="col-sm-3 col-form-label">Occupation History</label>
										<div class="col-sm-9">
											<input class="form-control" name="occupational_history" type="text" />
										</div>
									</div>
                                    <div class="mb-3 row">
										<label class="col-sm-3 col-form-label">Physical/Mental Exam</label>
										<div class="col-sm-9">
											<input class="form-control" name="physical_mental_examination" type="text" />
										</div>
									</div>
                                    <div class="mb-3 row">
										<label class="col-sm-3 col-form-label">Laboratory</label>
										<div class="col-sm-9">
											<input class="form-control" name="lab" type="text" />
										</div>
									</div>
                                    <div class="mb-3 row">
										<label class="col-sm-3 col-form-label">Assessment</label>
										<div class="col-sm-9">
											<input class="form-control" name="assessment" type="text" />
										</div>
									</div>
                                    <div class="mb-3 row">
										<label class="col-sm-3 col-form-label">Plan</label>
										<div class="col-sm-9">
											<input class="form-control" name="plan" type="text" />
										</div>
									</div>
                                    <div class="mb-3 row">
										<label class="col-sm-3 col-form-label">Remark</label>
										<div class="col-sm-9">
											<input class="form-control" name="remarks" type="text" />
										</div>
									</div>
									<div class="mb-3 row">
										<label class="col-sm-3 col-form-label">Medication</label>
										<div class="col-sm-3">
											<select class="form-control" name="medications">
                                                <option>Vitamin C</option>
                                                <option>Paracetamol</option>
                                            </select>
										</div>
										<label class="col-sm-3 col-form-label">Dosage/Frequency</label>
										<div class="col-sm-3">
											<select class="form-control" name="mstatus">
                                                <option>Days</option>
                                                <option>Weeks</option>
                                                <option>Months</option>
                                                <option>Single Dose</option>
                                                <option>Continous</option>
                                                <option>When needed</option>
                                            </select>
										</div>
									</div>
                                    
								</div>
							</div>
						</div>
						<div class="card-footer text-end">
							<div class="col-sm-9 offset-sm-3">
								<button class="btn btn-primary" type="submit">Save Details</button>
								<input class="btn btn-light" type="reset" value="Cancel" />
							</div>
						</div>
					</form>
				</div>
			
			</div>
		</div>
	</div>
	
	
	@push('scripts')
	@endpush

@endsection