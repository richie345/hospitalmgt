@extends('layouts.admin.master')

@section('title')Online Library
 View Readers
@endsection

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/datatables.css') }}">
@endpush

@section('content')
	@component('components.breadcrumb')
		@slot('breadcrumb_title')
			<h3>View Readers </h3>
		@endslot
		<li class="breadcrumb-item">Users</li>
		<li class="breadcrumb-item">Readers</li>
	@endcomponent
	
	<div class="container-fluid">
	    <div class="row">
	        <!-- Zero Configuration  Starts-->
	        <div class="col-sm-12">
	            <div class="card">
	               
	                <div class="card-body">
	                    <div class="table-responsive">
	                        <table class="display" id="basic-1">
	                            <thead>
	                                <tr>
                                        <th>#</th>
	                                    <th>Name</th>
	                                    <th>Email</th>
	                                    <th>Address</th>
	                                    <th>Gender</th>
                                        <th>Created On</th>
	                                    <th>Phone</th>
                                        <th>[Actions]</th>
	                                </tr>
	                            </thead>
	                            <tbody>
                                @foreach($readers as $data)
	                                <tr>
                                        <th>{{ $loop->iteration }}</th>
	                                    <td>{{ $data->firstname }} {{ $data->lastname }}</td>
	                                    <td>{{ $data->email }}</td>
	                                    <td>{{ $data->address }}</td>
	                                    <td>{{ $data->gender }}</td>
	                                    <td>{{ $data->created_at }}</td>
	                                    <td>{{ $data->mobilephone }}</td>
                                        <td>&nbsp;</td>
	                                </tr>
	                            @endforeach 
	                            </tbody>
	                        </table>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <!-- Zero Configuration  Ends-->
	     
	    </div>
	</div>

	
	@push('scripts')
	<script src="{{ asset('assets/js/datatable/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatables/datatable.custom.js') }}"></script>
	@endpush

@endsection