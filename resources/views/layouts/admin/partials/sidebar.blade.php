<header class="main-nav">
    <div class="sidebar-user text-center">
        <a class="setting-primary" href="javascript:void(0)"><i data-feather="settings"></i></a><img class="img-90 rounded-circle" src="{{asset('assets/images/dashboard/1.png')}}" alt="" />
        <div class="badge-bottom"><span class="badge badge-primary">New</span></div> 
        <a href="user-profile"> <h6 class="mt-3 f-14 f-w-600">{{ auth()->user()->name; }}</h6></a>
        
        
    </div>

 @if((auth()->user()->group_id)==500)
    <nav>
        <div class="main-navbar">
            <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
            <div id="mainnav">
                <ul class="nav-menu custom-scrollbar">
                    <li class="back-btn">
                        <div class="mobile-back text-end"><span>Back</span><i class="fa fa-angle-right ps-2" aria-hidden="true"></i></div>
                    </li>
                    <li class="sidebar-main-title">
                        <div>
                            <h6>Library</h6>
                        </div>
                    </li>
                    <li class="dropdown">
                        <a class="nav-link menu-title " href="javascript:void(0)"><i data-feather="home"></i><span>View</span></a>                  
                        <ul class="nav-submenu menu-content" >
                            <li><a href="{{route('available_books')}}" class="">Available Books</a></li>
                            <li><a href="{{route('myborrowed_books')}}" class="">Borrowed Books</a></li>
                            
                        </ul>
                    </li>            
                   
                    <li>
                        <a class="nav-link menu-title link-nav "  href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i data-feather="database"></i><span>{{ __('Logout') }}</span></a>
                    </li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </ul>
            </div>
            <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
        </div>
    </nav>
 @endif
 @if((auth()->user()->group_id)<>500)
<nav>
        <div class="main-navbar">
            <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
            <div id="mainnav">
                <ul class="nav-menu custom-scrollbar">
                    <li class="back-btn">
                        <div class="mobile-back text-end"><span>Back</span><i class="fa fa-angle-right ps-2" aria-hidden="true"></i></div>
                    </li>
                    <li class="sidebar-main-title">
                        <div>
                            <h6>Hospital</h6>
                        </div>
                    </li>
                    <li class="dropdown">
                        <a class="nav-link menu-title " href="javascript:void(0)"><i data-feather="home"></i><span>View</span></a>                  
                        <ul class="nav-submenu menu-content" >
                            <li><a href="{{route('home')}}" class="">Home</a></li>
                            
                        </ul>
                    </li>
                    <li class="sidebar-main-title">
                        <div>
                            <h6>Patient & Schedules</h6>
                        </div>
                    </li>
                    <li class="dropdown">
                        <a class="nav-link menu-title " href="javascript:void(0)"><i data-feather="box"></i><span>Patients</span></a>
                        <ul class="nav-submenu menu-content" >
                            <li><a href="{{ url('/add_patient') }}" class="">New Patient</a></li>
                            <li><a href="{{ url('/patients') }}" class="">View Patients</a></li>
                            <li><a href="{{ url('/borrowed_books') }}" class="">Borrowed Books</a></li>
                            
                        </ul>
                    </li>
                    
                    <li class="sidebar-main-title">
                        <div>
                            <h6>Outpatient Management</h6>
                        </div>
                    </li>
                    <li class="dropdown">
                        <a class="nav-link menu-title " href="javascript:void(0)"><i data-feather="sliders"></i><span>Users </span></a>
                        <ul class="nav-submenu menu-content" >
                            <li><a href="#" class="">Test</a></li>
                            
                        </ul>
                    </li>
                    <li class="sidebar-main-title">
                        <div>
                            <h6>Reports</h6>
                        </div>
                    </li>
                    <li class="dropdown">
                        <a class="nav-link menu-title " href="javascript:void(0)"><i data-feather="sliders"></i><span>Users </span></a>
                        <ul class="nav-submenu menu-content" >
                            <li><a href="#" class="">Test Report</a></li>
                            
                        </ul>
                    </li>
                                        
                   
                    <li>
                        <a class="nav-link menu-title link-nav " href="{{ route('logout') }}"><i data-feather="database"></i><span>Logout</span></a>
                    </li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </ul>
            </div>
            <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
        </div>
    </nav>
@endif
</header>
