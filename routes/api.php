<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('get_symptoms','HospitalApiController@get_symptoms')->name("get_symptoms");

Route::get('get_diagnosis/{symptoms}/{gender}/{year_of_birth}','App\Http\Controllers\HospitalApiController@get_diagnosis')->name("get_diagnosis/{symptoms}/{gender}/{year_of_birth}");

Route::get('getDiagnosis', 'App\Http\Controllers\HospitalApiController@getDiagnosis')->name("getDiagnosis");

