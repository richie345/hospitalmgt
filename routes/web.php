<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//Route::get('/patients', [App\Http\Controllers\PatientController::class, 'index'])->name('patients');
Route::get('/patients', [App\Http\Controllers\HospitalApiController::class, 'get_symptoms'])->name('patients');
Route::get('/borrowed_books', [App\Http\Controllers\BookController::class, 'borrowedBooks'])->name('borrowed_books');
Route::get('/add_patient', [App\Http\Controllers\PatientController::class, 'addPatientView'])->name('add_patient');
Route::post('/add_patient', [App\Http\Controllers\PatientController::class, 'store'])->name('addpatient');
Route::get('/view_patient/{patient_id}', [App\Http\Controllers\PatientController::class, 'show'])->name('view_patient');

Route::get('/add_patient_exam/{patient_id}/{sch_id}', [App\Http\Controllers\PatientExamController::class, 'addPatientExamView'])->name('add_patient_exam');
Route::post('/addpatientexam', [App\Http\Controllers\PatientExamController::class, 'store'])->name('addpatientexam');
Route::get('/change_validity/{id}', [App\Http\Controllers\PatientExamController::class, 'changeValidity'])->name('change_validity');

Route::post('/add_checkin', [App\Http\Controllers\EntryDetailController::class, 'store'])->name('add_checkin');

Route::post('/create_schedule', [App\Http\Controllers\ScheduleController::class, 'store'])->name('create_schedule');